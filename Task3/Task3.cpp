#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int binaryToDecimal(string);
int calculateSum(string);
int calculateSumFromFile(string);
void calculateSumTest(string);

int main()
{
	calculateSumTest("test.txt");
}

int calculateSum(string source)
{
	//TODO
	int sum = 0;
	string currentNumber = "";
	for (int i = 0; i < source.length(); i++)
	{
		if (source[i] >= '0' && source[i] <= '1')
		{
			currentNumber += source[i];
		}
		else
		{
			if (!currentNumber.empty())
			{
				sum += binaryToDecimal(currentNumber);
				currentNumber = "";
			}
		}
	}
	if (!currentNumber.empty())
	{
		sum += binaryToDecimal(currentNumber);
	}
	return sum;

}

int calculateSumFromFile(string fileName)
{
	//TODO
	ifstream file(fileName);
	string line;
	int sum = 0;
	while (getline(file, line))
	{
		sum += calculateSum(line);
	}
	file.close();
	return sum;
}

int binaryToDecimal(string binary)
{
	//TODO
	int decimal = 0;
	for (int i = 0; i < binary.length(); i++)
	{
		decimal = decimal * 2 + (binary[i] - '0');
	}
	return decimal;
}

void calculateSumTest(string fileName)
{
	bool result = calculateSum("1+-0100+*** 1000") == 13;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1000001+-1000+* 100** 1--- 0000001") == 79;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("10000000000011111+11+*** 1111000111") == 66537;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1111111111111000011111+-11111111111+*   1111** -22--- ") == 4195885;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSumFromFile(fileName) == 4262515;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
}

